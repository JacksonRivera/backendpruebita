import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Producto {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    @Column()
    price:number;

    @Column()
    img:string;

}