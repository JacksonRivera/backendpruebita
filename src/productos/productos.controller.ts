import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateProductoDto } from './dto/create-producto-dto';
import { ProductosService } from './productos.service';
import { fileFilter, renameImage } from 'src/helpers/images.helper';
import { diskStorage } from 'multer';

@Controller('productos')
export class ProductosController {

    constructor(private productosService:ProductosService){

    }
    @Post()
    create (@Body() createProductoDto:CreateProductoDto, @Res() response ){
       this.productosService.createProducto(createProductoDto).then(mensaje =>{
            response.status(HttpStatus.CREATED).json(mensaje);
       }
       ).catch( () => {
           response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la creación del producto'});
       });
    }
    @Get()
    getAll(@Res() response){
        this.productosService.getAll().then(productsList =>{
            response.status(HttpStatus.OK).json(productsList);
        }).catch(() =>{
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de los productos'});
        })
    }

    @Get('/:id')
    getProduct(@Res() response, @Param('id') id:number){
        this.productosService.getProduct(id).then(productsList =>{
            response.status(HttpStatus.OK).json(productsList);
        }).catch(() =>{
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de los productos'});
        })
    }


    @Put(':id')
    update(@Body() updateProducto:CreateProductoDto, @Res() response, @Param('id') idProducto){
        this.productosService.updateProducto(idProducto,updateProducto).then(mensaje =>{
            response.status(HttpStatus.OK).json(mensaje);
        }).catch(() =>{
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la actualización de los productos'});
        });
    }
    @Delete(':id')
    delete( @Res() response, @Param('id') idProducto){
        this.productosService.deleteProduct(idProducto).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }).catch(() =>{
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la eliminación de los productos'});
        })
    }

    @Post('upload/:productId')
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: './upload',
            filename: renameImage
        }),
        fileFilter:fileFilter
    }))
    uploadFile(@UploadedFile() file: Express.Multer.File, @Param('productId') idProducto) {     
        this.productosService.updateImgProduct(file.filename, idProducto)
        return file.filename;
    }
    
    @Get("getImage/:filename")
    async getFile(@Param("filename") filename: string, @Res() res: any) {
        res.sendFile(filename, { root: 'upload'});
    }
}