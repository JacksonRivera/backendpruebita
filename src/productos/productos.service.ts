import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductoDto } from './dto/create-producto-dto';
import { Producto } from './entities/producto.entity';

@Injectable()
export class ProductosService {

    constructor(
        @InjectRepository(Producto)
        private readonly productoRepository: Repository<Producto>,
    ){}


    async getAll(): Promise<Producto[]> {
        return await this.productoRepository.find();
    }

    async getProduct(id: number):Promise<Producto>{
        return await this.productoRepository.findOneBy({ id });
    }

    async createProducto(productoNuevo:CreateProductoDto):Promise<Producto> {
        const nuevo = new Producto();
        nuevo.id = productoNuevo.id;
        nuevo.name = productoNuevo.name;
        nuevo.price = productoNuevo.price;
        nuevo.img = productoNuevo.img;
        return this.productoRepository.save(nuevo);
    }
    async updateProducto(idProducto: any,productoActualizar:CreateProductoDto):Promise<Producto> {
        const productoUpdate = await this.productoRepository.findOneBy({id:idProducto});
        productoUpdate.id = productoActualizar.id;
        productoUpdate.name = productoActualizar.name;
        productoUpdate.price = productoActualizar.price;
        productoUpdate.img = productoActualizar.img;
        return this.productoRepository.save(productoUpdate);
    }
    async updateImgProduct(fileName:string, idProducto: any):Promise<Producto> {
        const productoUpdate = await this.productoRepository.findOneBy({id:idProducto});       
        productoUpdate.img = `http://localhost:3000/productos/getImage/${fileName}`;
        return this.productoRepository.save(productoUpdate);
    }

    async deleteProduct(idProducto:any):Promise<any>{
        return await this.productoRepository.delete(idProducto);
    }

}
